<?php
    session_start();

    if(isset($_POST['email'])){
        $form_complete = true;
        //walidacja nazwy użytkownika
        $username = $_POST['username'];

        if(strlen($username)<3 || strlen($username)>20){
            $form_complete = false;
            $_SESSION['e_username'] = "<div class='main_error'>Your username should have length between 3 and 20</div>";
        }
        if(!ctype_alnum($username)){
            $form_complete = false;
            $_SESSION['e_username'] = "<div class='main_error'>Your username should consist of letters and numbers</div>";
        }
        //walidacja imienia
        $name = $_POST['name'];

        if(!ctype_alpha($name)){
            $form_complete = false;
            $_SESSION['e_name'] = "<div class='main_error'>Your name should consist only letters</div>";
        }

        //walidacja adresu email
        $email = $_POST['email'];
        $email_filtered = filter_var($email, FILTER_SANITIZE_EMAIL);
        if(!filter_var($email_filtered, FILTER_VALIDATE_EMAIL) || ($email_filtered!=$email)){
            $form_complete = false;
            $_SESSION['e_email'] = "<div class='main_error'>Invaild e-mail address</div>";
        }

        //walidajca hasła
        $pass1 = $_POST['pass1'];

        if(strlen($pass1)<8){
            $form_complete = false;
            $_SESSION['e_pass'] = "<div class='main_error'>Password should consist minimum 8 characters</div>";
        }

        //hashowanie hasła
        $pass_hash = password_hash($pass1, PASSWORD_DEFAULT);


        //walidacja płci
        $gender = $_POST['gender'];
        if($gender == ""){
            $form_complete = false;
            $_SESSION['e_gender'] = "<div class='main_error'>You need to choose your gender</div>";
        }

        //czy regulamin został zatwierdzony
        if(!isset($_POST['rules'])){
            $form_complete = false;
            $_SESSION['e_rules'] = "<div class='main_error'>You need to accept our privacy policy</div>";
        }

        require_once "connect.php";
        mysqli_report(MYSQLI_REPORT_STRICT);

        try {
            $conn = new mysqli($servername, $db_username, $password, $dbname);
            if($conn->connect_errno!=0){
                throw new Exception(mysqli_connect_errno());
            } else {

                //Sprawdzanie, czy użytkownik o podanym emailu istniaje
                $result_email = $conn->query("SELECT id FROM users WHERE email='$email'");
                if(!$result_email) throw new Exception($conn->error);

                $email_check = $result_email->num_rows;
                if($email_check>0){
                    $form_complete = false;
                    $_SESSION['e_email'] = "<div class='main_error'>Account with this e-mail already exist</div>";
                }

                //Sprawdzanie, czy użytkownik o podanej nazwie użytkownika istniaje
                $result_username = $conn->query("SELECT id FROM users WHERE login='$username'");
                if(!$result_username) throw new Exception($conn->error);

                $username_check = $result_username->num_rows;
                if($username_check>0){
                    $form_complete = false;
                    $_SESSION['e_username'] = "<div class='main_error'>This username is taken</div>";
                }

                //Formularz jest prawidłowy
                $insertQuery = "INSERT INTO users (name, login, pass, email, gender) VALUES ('$name', '$username', '$pass_hash', '$email', '$gender')";
                if($form_complete == true){
                    if($conn->query($insertQuery)){
                        $_SESSION['register_complete'] = true;
                        header('Location: welcome.php');
                    } else {
                        throw new Exception($conn->error);
                    }
                }

                $conn->close();
            }
        } catch (Exception $e) {
            echo "Server error. Sorry for problems. Please comeback later.";
            echo '<br/>Developer info: '.$e; // wyłączyć przy wrzucaniu na prawdziwy serwer
        }
    }

?>
<html>
    <head>
        <title>Register</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="styles/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="styles/css/register.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap"
            rel="stylesheet">
        <script src="https://hcaptcha.com/1/api.js" async defer></script>
    </head>
    <body>
        <div id="register_main">
            <form id="register_form" method="post">
            <div id="register_form_title">
                <h1>Sign up</h1>
            </div>
            <div id="names">
                <input type="text" name="name" placeholder=" Your name" required/>
                <input type="text" name="username" placeholder=" Username" required/>
            </div>
                <input type="email" name="email" placeholder=" example@example.com" required/>
                <input type="password" name="pass1" placeholder=" at least 8 characters" required/>
                <div id="container">
                    <select name="gender" id="gender">
                        <option value="">Select gender</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <input type="checkbox" id="rules" name="rules" >
                    <label class="rules" for="rules">I accept the <a href="">rules</a></label>
                </div>
                <div id="register_form_buttons">
                    <button class="button" type="submit"><img src="assets/img/icons/user-plus.svg">&nbspCreate account</button><br/>
                   <span style="color: #808080; font-size: 1em">or&nbsp</span><a href="index.php" style="font-weight: 600;">Sign in</a></span>
                </div>
                <div class="h-captcha" data-sitekey="4a293a5e-d456-4597-b161-3de091c6553e"></div>
            </form>
            <img src="assets/img/illustrations/blank_canvas.svg"/>
            <?php
            if(isset($_SESSION['e_username'])) {
                echo $_SESSION['e_username'];
                unset($_SESSION['e_username']);
            }
            if(isset($_SESSION['e_name'])) {
                echo $_SESSION['e_name'];
                unset($_SESSION['e_name']);
            }
            if(isset($_SESSION['e_email'])) {
                echo $_SESSION['e_email'];
                unset($_SESSION['e_email']);
            }
            if(isset($_SESSION['e_pass'])) {
                echo $_SESSION['e_pass'];
                unset($_SESSION['e_pass']);
            }
            if(isset($_SESSION['e_gender'])) {
                echo $_SESSION['e_gender'];
                unset($_SESSION['e_gender']);
            }
            if(isset($_SESSION['e_rules'])) {
                echo $_SESSION['e_rules'];
                unset($_SESSION['e_rules']);
            }
            ?>
        </div>
        <script>
            const select = document.querySelector("#gender");
            console.log(select.value);
            if(select.value = ""){
                select.style.color = "black";
            }
            </script>
    </body>
</html>
