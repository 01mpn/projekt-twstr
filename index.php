<?php
    session_start();

    //sprawdzanie czy użytkownik jest zalogowany, jeśli jest, przenosi go do profile.php
    if(isset($_SESSION['logged']) && ($_SESSION['logged'] == true)) {
        header('Location: profile.php');
        exit();
    }
?>
<html>
    <head>
        <title>Sign in</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="styles/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="styles/css/index.css"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap"
            rel="stylesheet">
    </head>
    <body>
        <div id="index_main">
            <img id="index_photo" src="assets/img/illustrations/login.svg"/>
            <form id="index_form" action="login.php" method="post">
                <div id="index_form_title">
                    <h1>Sign in</h1>
                </div>
                <input type="text" name="login" placeholder=" Username" required/>
                <input type="password" name="pass" placeholder=" Password" required/>
                <div id="index_form_buttons">
                    <button class="button" type="submit"><img src="assets/img/icons/login.svg">&nbspLog in</button>
                    <span style="color: #808080; font-size: 1em">or&nbsp</span><a href="register.php">Sign up</a></p>
                </div>
            </form>
            <?php
                if(isset($_SESSION['error'])) echo $_SESSION['error'];
            ?>
        </div>
        <script type="text/javascript"></script>
    </body>
</html>
