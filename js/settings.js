const details = document.querySelector('#settings_details');
const privacy = document.querySelector('#settings_privacy');
const images = document.querySelector('#settings_pictures');
const buttonDetails = document.querySelector('.buttonDetails');
const buttonImages = document.querySelector('.buttonImages');
const buttonPrivacy = document.querySelector('.buttonPrivacy');

//dekstop menu
const showDetails = () => {
    details.style.display = "flex";
    privacy.style.display = "none";
    images.style.display = "none";
    buttonDetails.classList.add("active");
    buttonPrivacy.classList.remove("active");
    buttonImages.classList.remove("active");
}

const showPrivacy = () => {
    privacy.style.display = "flex";
    details.style.display = "none";
    images.style.display = "none";
    buttonPrivacy.classList.add("active");
    buttonDetails.classList.remove("active");
    buttonImages.classList.remove("active")
}

const showPictures = () => {
    privacy.style.display = "none";
    details.style.display = "none";
    images.style.display = "flex";
    buttonPrivacy.classList.remove("active");
    buttonDetails.classList.remove("active");
    buttonImages.classList.add("active");
}

buttonDetails.addEventListener('click', showDetails, false);
buttonPrivacy.addEventListener('click', showPrivacy, false);
buttonImages.addEventListener('click', showPictures, false);

//mobile menu
const closeButton = document.querySelector('#settings_hamburger_close_button');
const openButton = document.querySelector('#settings_hamburger_open_button');
const mobileMenu = document.querySelector('#settings_hamburger');

const buttonMobileDetails = document.querySelector('.buttonMobileDetails');
const buttonMobileImages = document.querySelector('.buttonMobileImages');
const buttonMobilePrivacy = document.querySelector('.buttonMobilePrivacy');

const openMenu = () => {
    mobileMenu.style.animation = "slideIn 0.4s";
    mobileMenu.style.animationFillMode = "forwards";
    details.style.filter = "grayscale(1);"
}

const closeMenu = () => {
    mobileMenu.style.animation = "slideOut 0.5s";
    mobileMenu.style.animationFillMode = "forwards";
}

const showMobileDetails = () => {
    details.style.display = "flex";
    privacy.style.display = "none";
    images.style.display = "none";
    buttonMobileDetails.classList.add("active");
    buttonMobilePrivacy.classList.remove("active");
    buttonMobileImages.classList.remove("active");
    mobileMenu.style.animation = "slideOut 0.5s";
    mobileMenu.style.animationFillMode = "forwards";
}

const showMobilePrivacy = () => {
    privacy.style.display = "flex";
    details.style.display = "none";
    images.style.display = "none";
    buttonMobilePrivacy.classList.add("active");
    buttonMobileDetails.classList.remove("active");
    buttonMobileImages.classList.remove("active")
    mobileMenu.style.animation = "slideOut 0.5s";
    mobileMenu.style.animationFillMode = "forwards";
}

const showMobilePictures = () => {
    privacy.style.display = "none";
    details.style.display = "none";
    images.style.display = "flex";
    buttonMobilePrivacy.classList.remove("active");
    buttonMobileDetails.classList.remove("active");
    buttonMobileImages.classList.add("active");
    mobileMenu.style.animation = "slideOut 0.5s";
    mobileMenu.style.animationFillMode = "forwards";
}



closeButton.addEventListener('click', closeMenu);
openButton.addEventListener('click', openMenu);
buttonMobileDetails.addEventListener('click', showMobileDetails);
buttonMobilePrivacy.addEventListener('click', showMobilePrivacy);
buttonMobileImages.addEventListener('click', showMobilePictures);