<?php
    session_start();
    require_once "connect.php";
    $userId = $_SESSION['id'];
    $currentDate = date('d.m.Y');

    $conn = new mysqli($servername, $db_username, $password, $dbname);
    try {
        if($conn->connect_errno != 0){
            throw new Exception(mysqli_connect_errno());
        } else {
            $queryPosts = "SELECT posts.*, users.name, users.login
            FROM posts, users
            RIGHT JOIN friends ON friends.user2 = users.id
            WHERE friends.user1 = $userId AND posts.author_id != $userId";

            //zapytanie dodające posty
            //INSERT INTO posts (text, date, author_id) VALUES ($userId, $text, $date)
        }
    } catch (Exception $e){
        echo $e;
    }

?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>#PROJECT_TWSTR</title>
    <link rel="stylesheet" type="text/css" href="styles/css/index2.css"/>
</head>
<body>
    <form method="POST">
        <textarea name="text" placeholder="What is on your mind?"></textarea><br/>
        <button type="submit" name="submit">Post</button>
    </form>
    <?php
        $result = $conn->query($queryPosts);
        while(($row = $result->fetch_assoc()) != 0){
            echo "<div class='index_post'>";
                echo "<div class='index_post_header'>";
                echo $row['name']."@".$row['login']." ".$row['date']."<br/>";
                echo "<div class='post_content'>";
                echo $row['text']."<br/>";
            echo "</div>";
        }
    ?>
</body>
</html>