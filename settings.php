<?php
    session_start();

        //sprawdzanie czy użytkownik jest zalogowany do profilu, jeśli nie, przenosi go do index.php
        if(!isset($_SESSION['logged'])){
            header('Location: index.php');
            exit();
        }
?>
    <?php
        require_once 'connect.php';
        $userId = $_SESSION['id'];
        $currentPass = $_SESSION['pass'];

        try {

            $conn = new mysqli($servername, $db_username, $password, $dbname);
            if($conn->connect_errno != 0){
                throw new Exception(mysqli_connect_errno());
            } else {

                $querySelect = "SELECT email FROM users";
                $resultSelect = $conn->query($querySelect);
                $rowSelect = $resultSelect->fetch_assoc();
                //przypisanie zmiennych sesyjnych do zmiennych lokalnych
                $userName = $_SESSION['name'];
                $userLogin = $_SESSION['login'];
                $userEmail = $_SESSION['email'];



                //ustawienia dotyczące szczegółów konta
                if(isset($_POST['submitDetails'])){ //jeżeli zmienna się utworzy, zostaną wykonane instrukcje niżej

                    $detailsFormComplete = true;
                    $userNameChanged = $_POST['nameChanged'];
                    //$userLoginChanged = $_POST['loginChanged'];


                    //walidaca adresu email
                    $userEmailChanged = $_POST['emailChanged'];
                    //przefiltrowywanie adresu email
                    $email_filtered = filter_var($userEmailChanged, FILTER_SANITIZE_EMAIL);
                    //sprawdzanie czy przefiltrowany email i wpisany email są takie same
                    if(!filter_var($email_filtered, FILTER_VALIDATE_EMAIL) || ($email_filtered!=$userEmailChanged)){
                        $_SESSION['settings_email_error'] = "<div class='main_error'>Invaild e-mail address</div>";
                        $detailsFormComplete = false;
                    }

                    //wyciąganie z bazy informacji, czy taki adres email jest już wpisany
                    $result_email = $conn->query("SELECT id FROM users WHERE email='$userEmailChanged'");
                    if(!$result_email) throw new Exception($conn->error);
                    $email_check = $result_email->num_rows;
                    //sprawdzanie czy konto z podanym adresem email już istnieje
                    if($email_check>0){
                        if($userEmailChanged != $userEmail){
                            $_SESSION['settings_email_error'] = "<div class='main_error'>Account with this e-mail already exist</div>";
                            $detailsFormComplete = false;
                        }
                    }

                    //walidacja imienia
                    $userNameChanged = htmlentities($userNameChanged, ENT_QUOTES, "UTF-8");
                    if(!ctype_alpha($userNameChanged)){
                        $_SESSION['settings_name_error'] = "<div class='main_error'>Your name should contain only letters</div>";
                        $detailsFormComplete = false;
                    }

                    //jeżeli pola formularza są takie same jak informacje w bazie danych
                    if($userName === $userNameChanged && $userEmail === $userEmailChanged){
                        $_SESSION['settings_error'] = '<div class="main_error">You need to change something!</div>';
                        $detailsFormComplete = false;
                    }

                    //aktualizowanie profulu według wprowadzonych danych
                    $queryUpdateProfile = "UPDATE users SET name='$userNameChanged', email='$userEmailChanged' WHERE id='$userId'";
                    if($detailsFormComplete == true){
                        if($conn->query($queryUpdateProfile)){
                            header("Location: profile.php"); //przenosi do strony profilu
                            $_SESSION['name'] = $userNameChanged;  //zmienia zmienną sesyjną na wartosći wpisane w formularzu
                            $_SESSIONN['email'] = $userEmail; // jak wyżej
                            $_SESSION['settings_success'] = '<div class="main_success">Your account has been updated</div>'; //tworzy zmienną sesyjną z powiadomieniem o pomyślnej zmianie danych
                        } else {
                            throw new Exception(mysqli_connect_error());
                        }
                    }
                }

                //ustawienia obrazóww

                if(isset($_POST['submitPictures'])){
                    $img = $_FILES['avatar'];

                    $imgName = $_FILES['avatar']['name'];
                    $imgTmpName = $_FILES['avatar']['tmp_name'];
                    $imgSize = $_FILES['avatar']['size'];
                    $imgError = $_FILES['avatar']['error'];

                    $imgExt = explode('.', $imgName);
                    $imgActualExt = strtolower(end($imgExt));

                    $allowedExt = array('jpg','jpeg','png');

                    if(in_array($imgActualExt, $allowedExt)){
                        if($imgError === 0){
                            if($imgSize < 10000000) {
                                $imgNewName = uniqid('', true).".".$imgActualExt;
                                $imgDest = "uploads/avatars/".$imgNewName;

                                $_SESSION['upload_picture_success'] = "<div class='main_success'>Your image was uploaded succesfuly!</div>";
                                move_uploaded_file($imgTmpName, $imgDest);
                                header("Location: profile.php");
                            } else {
                                $_SESSION['size_picture_error'] = "<div class='main_error'>Your file is to big!</div>";
                            }
                        } else {
                            $_SESSION['e_picture_error'] = "<div class='main_error'>There was an error! Please try again later</div>";
                        }
                    } else {
                        $_SESSION['ext_picture_error'] = "<div class='main_error'>You cannot upload files of this type</div>";
                    }

                }

                //ustawienia prywatności
                if(isset($_POST['submitPassword'])) {
                    $privacyFormComplete = true;
                        //walidacja starego hasła
                        $oldPassword = $_POST['oldPassword'];
                            //sprawdzanie czy wpisane obecne hasło zgadza się z tym z bazy danych
                            if(password_verify($oldPassword, $currentPass) == false){
                                $privacyFormComplete = false;
                                $_SESSION['old_pass_error'] = "<div class='main_error'>You typed a wrong password</div>";
                            }
                        //walidacja nowego hasła
                        $newPass1 = $_POST['newPassword'];
                        $newPass2 = $_POST['newPasswordRepeat'];
                            //sprawdzanie czy hasło ma przynajmniej 8 znaków
                            if($newPass1>8) {
                                $privacyFormComplete = false;
                                $_SESSION['new_pass_error'] = "<div class='main_error'>Password should consist minimum 8 characters</div>";
                            }
                            //sprawdzanie czy wpisane hasła są takie same
                            if($newPass1!=$newPass2) {
                                $privacyFormComplete = false;
                                $_SESSION['new_pass_error'] = "<div class='main_error'>Passwords don't match</div>";
                            }
                            //sprawdzanie czy pola haseł nie są puste
                            if($newPass1 == null && $newPass2 == null){
                                $privacyFormComplete = false;
                                $_SESSION['empty_pass_error'] = "<div class='main_error'>Password fields can't be empty</div>";
                            }
                            //hashowanie nowego hasła
                            $passChanged = password_hash($newPass1, PASSWORD_DEFAULT);

                            if($privacyFormComplete == true){
                                $queryChangePassword = "UPDATE users SET pass='$passChanged' WHERE id='$userId'";
                                if($conn->query($queryChangePassword)){
                                    header("Location: profile.php");
                                    $_SESSION['pass_changed_success'] = '<div class="main_success">Your password has been changed</div>'; //tworzy zmienną sesyjną z powiadomieniem o pomyślnej zmianie danych
                                } else {
                                    throw new Exception($conn->error);
                                }
                            }

                        }

            }

            $conn->close();
        } catch (Exception $e) {
            echo "Server error. Sorry for problems. Please comeback later.";
            echo '<br/>Developer info: '.$e; // wyłączyć przy wrzucaniu na prawdziwy serwer
        }
    ?>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="styles/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="styles/css/settings.css"/>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet">
    <title>Settings</title>
</head>
<body>
    <div id="settings">
    <div id='settings_sidebar'>
            <div id='settings_sidebar_menu'>
                <ul>
                <li><h1 style="color: white; margin-top: 0;"><center>Settings</center></h1></li>
                <li><a class='button buttonDetails active'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-id' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <rect x='3' y='4' width='18' height='16' rx='3' />
                    <circle cx='9' cy='10' r='2' />
                    <line x1='15' y1='8' x2='17' y2='8' />
                    <line x1='15' y1='12' x2='17' y2='12' />
                    <line x1='7' y1='16' x2='17' y2='16' />
                </svg>&nbspYour details</a></li>
                <li><a class='button buttonImages'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-photo' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <line x1='15' y1='8' x2='15.01' y2='8' />
                    <rect x='4' y='4' width='16' height='16' rx='3' />
                    <path d='M4 15l4 -4a3 5 0 0 1 3 0l5 5' />
                    <path d='M14 14l1 -1a3 5 0 0 1 3 0l2 2' />
                </svg>&nbspPictures</a></li>
                <li><a class='button buttonPrivacy'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-shield-lock' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <path d='M12 3a12 12 0 0 0 8.5 3a12 12 0 0 1 -8.5 15a12 12 0 0 1 -8.5 -15a12 12 0 0 0 8.5 -3' />
                    <circle cx='12' cy='11' r='1' />
                    <line x1='12' y1='12' x2='12' y2='14.5' />
                </svg>&nbspPrivacy</a></li>
                </ul>
            </div>
       </div>
        <div id="settings_hamburger">
            <a id="settings_hamburger_close_button">
                <img src="assets/img/icons/x.svg"/>
            </a>
            <ul>
                <li><h1 style="color: white; margin-top: 0;"><center>Settings</center></h1></li>
                <li><a class='button buttonMobileDetails active'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-id' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <rect x='3' y='4' width='18' height='16' rx='3' />
                    <circle cx='9' cy='10' r='2' />
                    <line x1='15' y1='8' x2='17' y2='8' />
                    <line x1='15' y1='12' x2='17' y2='12' />
                    <line x1='7' y1='16' x2='17' y2='16' />
                </svg>&nbspYour details</a></li>
                <li><a class='button buttonMobileImages'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-photo' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <line x1='15' y1='8' x2='15.01' y2='8' />
                    <rect x='4' y='4' width='16' height='16' rx='3' />
                    <path d='M4 15l4 -4a3 5 0 0 1 3 0l5 5' />
                    <path d='M14 14l1 -1a3 5 0 0 1 3 0l2 2' />
                </svg>&nbspPictures</a></li>
                <li><a class='button buttonMobilePrivacy'><svg xmlns='http://www.w3.org/2000/svg' class='icon icon-tabler icon-tabler-shield-lock' width='24' height='24' viewBox='0 0 24 24' stroke-width='1.5' stroke='#ffffff' fill='none' stroke-linecap='round' stroke-linejoin='round'>
                    <path stroke='none' d='M0 0h24v24H0z' fill='none'/>
                    <path d='M12 3a12 12 0 0 0 8.5 3a12 12 0 0 1 -8.5 15a12 12 0 0 1 -8.5 -15a12 12 0 0 0 8.5 -3' />
                    <circle cx='12' cy='11' r='1' />
                    <line x1='12' y1='12' x2='12' y2='14.5' />
                </svg>&nbspPrivacy</a></li>
            </ul>
        </div>
        <a id="settings_hamburger_open_button"><img src="assets/img/icons/align-left.svg"/></a>
    <div id='settings_details'>
        <form method='post'>
            <h1>Profile details</h1>
            <label for='email' id='label3'>Username</label>
            <input type='text' name='loginChanged' value='<?php echo $userLogin ?>' disabled/><br/>
            <label for='name' id='label1'>Your name</label>
            <input type='text' name='nameChanged' value='<?php echo $userName ?>' required/><br/>
            <label for='login' id='label2'>E-mail</label>
            <input type='email' name='emailChanged' value='<?php echo $userEmail ?>' required/><br/>
            <button class='button' type='submit' name='submitDetails'><img src='assets/img/icons/disc-floppy.svg'>&nbspSave profile</button>
        </form>
    </div>
    <div id="settings_privacy">
        <form method="post">
        <h1>Password settings</h1>
            <label for="oldPassword">Current password</label>
            <input type="password" name="oldPassword"/>
            <label for="newPassword">New password</label>
            <input type="password" name="newPassword"/>
            <label for="newPasswordRepeat">Repeat new password</label>
            <input type="password" name="newPasswordRepeat"/>
            <button class="button" name="submitPassword" type="submit"><img src="assets/img/icons/disc-floppy.svg"/>&nbspSave password</button>
        </form>
    </div>
    <div id="settings_pictures">
        <form method="post" enctype="multipart/form-data">
        <h1>Profile pictures</h1>
                <label for="avatar">Profile photo</label>
                <input class="button-upload"type="file" name="avatar"/>
            <p style="font-size: 14px; margin-top: 0;">Your photo must weight max. 1Mb and be converted to .jpg, .jpeg or .png</p>
            <button class="button" name="submitPictures" type="submit"><img src="assets/img/icons/cloud-upload.svg"/>&nbspUpload picture</button>
        </form>
    </div>
    <?php

        if(isset($_SESSION['settings_error'])){
            echo $_SESSION['settings_error'];
            unset($_SESSION['settings_error']);
        };
        if(isset($_SESSION['settings_email_error'])){
            echo $_SESSION['settings_email_error'];
            unset($_SESSION['settings_email_error']);
        };
        if(isset($_SESSION['settings_name_error'])){
            echo $_SESSION['settings_name_error'];
            unset($_SESSION['settings_name_error']);
        };
        if(isset($_SESSION['old_pass_error'])) {
            echo $_SESSION['old_pass_error'];
            unset($_SESSION['old_pass_error']);
        }
        if(isset($_SESSION['new_pass_error'])) {
            echo $_SESSION['new_pass_error'];
            unset($_SESSION['new_pass_error']);
        }
        if(isset($_SESSION['empty_pass_error'])) {
            echo $_SESSION['empty_pass_error'];
            unset($_SESSION['empty_pass_error']);
        }
        if(isset($_SESSION['ext_picture_error'])) {
            echo $_SESSION['ext_picture_error'];
            unset($_SESSION['ext_picture_error']);
        }
        if(isset($_SESSION['size_picture_error'])) {
            echo $_SESSION['size_picture_error'];
            unset($_SESSION['size_picture_error']);
        }
        if(isset($_SESSION['e_picture_error'])) {
            echo $_SESSION['e_picture_error'];
            unset($_SESSION['e_picture_error']);
        }
    ?>
    <footer>
        <a class="button" style="margin-bottom: 3vh;" href='profile.php'><img src='assets/img/icons/arrow-back-up.svg'>&nbspGo back</a>
    </footer>
    <script src="js/settings.js"></script>
    <script src="js/main.js"></script>
    </div>
</body>
</html>